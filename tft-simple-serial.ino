#include "MCUFRIEND_kbv.h"
#include "Fonts/FreeMono12pt7b.h"
//#include "Fonts/Org_01.h"

#define BG TFT_BLACK
#define FG TFT_WHITE

MCUFRIEND_kbv tft;
char cmd;

void setup() {
	uint16_t ID = tft.readID();
	tft.begin(ID);
	tft.setRotation(3);
	tft.fillScreen(BG);
	tft.setTextColor(FG);
	//tft.setFont(&FreeMono18pt7b); // 15x7 chars
	tft.setFont(&FreeMono12pt7b); // ~22x10 chars
	//tft.setFont(&FreeMono9pt7b); // ~30x12 chars
	//tft.setFont(&Org_01); // v. small chars
	tft.setCursor(0, 24);
	tft.write("Initialized");

	Serial.begin(9600);
}

void loop() {
	cmd = read();
	//tft.print(cmd, HEX);
	//return;
	switch (cmd) {
		case -1:
			break;
		case 0x0C:
			tft.fillScreen(BG);
			tft.setTextColor(FG);
			tft.setCursor(0, 24);
			break;
		case 0x1B:
			//tft.print('E');
			parse_esc_seq();
			break;
		case 0x0D:
			break;
		default:
			tft.write(cmd);
	}
}

void parse_esc_seq() {
	int code = 0;

	if (read() != '[') return;

	while ((cmd = read()) != 'm') {
		if (cmd >= '0' && cmd <= '9') {
			code *= 10;
			code += cmd - '0';
		} else
			return;
	}
	//tft.print(code);
	process_esc_seq(code);
	return;
}

void process_esc_seq(int code) {
	switch (code) {
		case 37: //bold
			break;
		case 32: //green
			tft.setTextColor(TFT_GREEN);
			break;
		case 31: //red
			tft.setTextColor(TFT_RED);
			break;
		case 0: //reset
			tft.setTextColor(FG);
			break;
	}
}

char read() {
	while (!Serial.available()) {}
	return Serial.read();
}

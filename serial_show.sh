#!/bin/bash

# command to run
COMMAND="date"

# serial device for Arduino
TTY="/dev/ttyACM0"
#TTY="/dev/ttyUSB0"
#TTY="/dev/stdout"

# speed of serial device
SPEED=9600

# delay between runs of COMMAND
DELAY=10

# set serial device speed and disable closing the device after each transfer
stty -F $TTY $SPEED -hupcl

while [ 0 -lt 1 ]
do
  OUT=`$COMMAND`

# clear display
  echo -ne "\x0C" > $TTY
  echo "$OUT" > $TTY
#  $COMMAND > $TTY
  sleep $DELAY
done

Trivial sketch to run Arduino-powered display
=============================================

The sketch has two versions. One works with TFT displays supported by MCUFRIEND_kbv library. The other works with LCD displays supported by LiquidCrystal library (like HD44780 2x16 or 4x20 panels).

Both sketches support limited parsing of escape sequences. LCD version just silently ignores them, while TFT version tries to apply them, but at the moment it only supports green and red text. Adding other colors is straighforward.

Sketches may need tuning for specific devices (font, colors, pinout).

You could also try to use more elaborate sketches like https://github.com/ZivaVatra/ArduLCD which work with LCDd software. In my opinion it is an overkill for simple applications though.

# Usage

Upload a sketch to Arduino. It should display "Initialized". Send any text you wish to display to serial port. It is usually something like /dev/ttyACM0 or /dev/ttyUSB0 (depending on the board).

```
echo "Testing Arduino display" > /dev/tty
```

To clear the display send "Form Feed" control character to the serial port (represented as: FF, 0x0C, \f, ^L)

```
echo -ne "\x0C" > $TTY
```

The script `serial_show.sh` is a simple watch-like tool to run a command periodically and push output to the serial port.

# Notes

 * Some boards and display combinations are prone to errors and work better with different connection speeds. When a speed is too low or too high errors in transmission may appear.
 * I also noticed that some use cases have problems with `serial_show.sh` attempt to first run the command and then output it to serial device. For some reason simpler approach works fine. To try it comment lines

```
  OUT=`$COMMAND`
  ...
  echo "$OUT" > $TTY
```

and uncomment:

```
  $COMMAND > $TTY
```

# Links:

 * https://github.com/prenticedavid/MCUFRIEND_kbv
 * https://www.arduino.cc/en/Reference/LiquidCrystal
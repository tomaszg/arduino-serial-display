#include <LiquidCrystal.h>

// #define BG TFT_BLACK
// #define FG TFT_WHITE

const int LCDW = 20;
const int LCDH = 4;

const int rs = 12, en = 11, d4 = 10, d5 = 9, d6 = 8, d7 = 7;
LiquidCrystal tft(rs, en, d4, d5, d6, d7);

char cmd;
char row;

void setup() {
	tft.begin(LCDW, LCDH);
	Serial.begin(57600);
	tft.display();
	tft.noCursor();
	tft.clear();
	tft.write("Initialized");
	row = 0;
	tft.home();
}

void loop() {
	cmd = read();
	switch (cmd) {
		case -1:
			break;
		case 0x0C:
			tft.clear();
			row = 0;
			tft.home();
			break;
		case 0x1B:
			//tft.print('E');
			parse_esc_seq();
			break;
		case 0x0A:
			row++;
			tft.setCursor(0, row);
			break;
		case 0x0D:
			break;
		default:
			tft.print(cmd);
	}
}

void parse_esc_seq() {
	int code = 0;

	if (read() != '[') return;

	while ((cmd = read()) != 'm') {
		if (cmd >= '0' && cmd <= '9') {
			code *= 10;
			code += cmd - '0';
		} else
			return;
	}
	//tft.print(code);
	//process_esc_seq(code);
	return;
}

/*
void process_esc_seq(int code) {
  switch (code) {
    case 37: //bold
      break;
    case 32: //green
      tft.setTextColor(TFT_GREEN);
      break;
    case 31: //red
      tft.setTextColor(TFT_RED);
      break;
    case 0: //reset
      tft.setTextColor(FG);
      break;
  }
}
*/

char read() {
	int x;
	while ((x = Serial.read()) == -1) {}
	return x;
}
